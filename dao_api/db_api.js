var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/demo";

// insert data
function insertCollection (dbName, collectionName, myobj) {	 
	MongoClient.connect(url, function (err, client) {
	    if (err) throw err;

	    var db = client.db(dbName);

	    db.collection(collectionName).insertOne(myobj, function(err, res) {
	        if (err) throw err;
	        console.log("文档插入成功");
	        client.close();
	    });
	});
}


// find data 
function findOne(dbName, collectionName, whereStr) { 
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var dbo = db.db(dbName);
        //var whereStr = {"name":'菜鸟教程'};  // 查询条件
        dbo.collection(collectionName).find(whereStr).toArray(function(err, result) {
            if (err) throw err;
            // print reuslt 
            // console.log(result);
            // var tmp = result;
            db.close();
            // return result 
            console.log(JSON.stringify(result));
    		return JSON.stringify(result);
        });
    });
}

// find all data
function findAll(dbName, collectionName){
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var dbo = db.db(dbName);

        dbo.collection(collectionName). find({}).toArray(function(err, result) { 
        // 返回集合中所有数据
            if (err) throw err;
            console.log(result);
            db.close();
    		return result;
        });
    });
}


// below are the testing code 

var dbName = "test";
var collectionName = "site";
var myobj = {"name": "zhuboshi", "password": "xxx" };

var whereStr = {"name": 'zhuboshi'};

// test three API

insertCollection(dbName, collectionName, myobj);

// var result = findOne(dbName, collectionName, whereStr);
// console.log(result.password);

// here couldn't print cause in the findOne(), it can't return the result
// console.log(findOne(dbName, collectionName, whereStr));

// findAll(dbName, collectionName);