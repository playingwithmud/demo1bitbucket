<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>City Night</title>

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Poppins:400,600" rel="stylesheet">


    <!-- favicon and touch icons -->
    <link rel="shortcut icon" href="assets/images/favicon.png" >
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">

    <!--<link rel="apple-touch-icon" href="apple-touch-icon.png">-->

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/fontAwesome.css">
    <link rel="stylesheet" href="css/hero-slider.css">
    <link rel="stylesheet" href="css/owl-carousel.css">
    <link rel="stylesheet" href="css/tempo1.css">
    <link rel="stylesheet" href="css/lightbox.css">
    <!-- Bootstrap -->
    <!--<link href="assets/css/bootstrap.css" rel="stylesheet">-->
    <link href="assets/css/theme.css" rel="stylesheet">

    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <script src="js/jquery-2.1.1.min.js" type="text/javascript"></script>
    <script src="js/cartmain.js"></script> <!-- Resource jQuery -->

    <link rel="stylesheet" type="text/css" href="css/reset.css" />
    <link rel="stylesheet" href="css/cartstyle.css">
</head>
<body class="">



<div class="header">
    <div class="container">
        <nav class="navbar navbar-inverse" role="navigation">
            <div class="navbar-header">
                <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand scroll-top"><em>M</em>UD</a>
            </div>
            <!--/.navbar-header-->
            <div id="main-nav" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a style="font-size: 18px;" href="#" class="scroll-top">Home</a></li>
                    <li><a style="font-size: 18px;" href="#" class="scroll-profile" data-id="profile">Profile</a></li>
                    <li><a style="font-size: 18px;" href="#" class="logout" data-id="logout">Logout</a></li>
                </ul>
            </div>
            <!--/.navbar-collapse-->
        </nav>
        <!--/.navbar-->
    </div>
    <!--/.container-->
</div>
<!--/.header-->


<div class="parallax-content baner-content" id="home">
    <div class="container">
        <div class="text-content">
            <h2>Awesome <span>Algorithm</span> Trading <em>Website</em></h2>
            <!--<p>Phasellus aliquam finibus est, id tincidunt mauris fermentum a. In elementum diam et dui congue, ultrices bibendum mi lacinia. Aliquam lobortis dapibus nunc, nec tempus odio posuere quis. </p>-->
            <a style="font-size: 25px; position:absolute; border-color: white; margin-top: 90px;margin-left: -50px" href="#" class="scroll-link" data-id="start">Get Started</a>
        </div>
    </div>
</div>





<div id="advance-search" class="main-page clearfix ">
    <div class="container">
        <form action="#" id="adv-search-form" class="clearfix">
            <fieldset>
                <select name="type" id="main-location">
                    <option value="">All type</option>
                    <option value="AI"> AI</option>
                    <option value="Databse"> Databse</option>
                    <option value="Cloud"> Cloud</option>
                    <option value="Game"> Game</option>
                </select>
                <select name="sub-location" id="property-sub-location">
                    <option value="">Any Complexity</option>
                    <option value="brickell" > very easy</option>
                    <option value="brickyard" > easy</option>
                    <option value="bronx" > Medium</option>
                    <option value="brooklyn" > Hard</option>
                    <option value="coconut-grove" > Very Hard</option>
                </select>
                <select id="property-status" name="status">
                    <option value="">All posted date</option>
                    <option value="for-rent"> One day ago</option>
                    <option value="for-sale"> two days ago</option>
                    <option value="foreclosures">three days ago</option>
                    <option value="new-costruction">A week ago</option>
                    <option value="new-listing"> A month ago</option>
                </select>
                <select id="property-type" name="type" >
                    <option value="">No Time limit</option>
                    <option value="apartment">One day</option>
                    <option value="condo"> Two days</option>
                    <option value="farm"> Three days</option>
                    <option value="loft"> A week</option>
                    <option value="lot"> A Month</option>
                </select>
                <select name="bedrooms" id="property-beds">
                    <option value="">With data</option>
                    <option value="1">Without data</option>
                </select>
                <select name="bathrooms" id="property-baths">
                    <option value="">PornHUB</option>
                    <option value="1">Kingscross</option>
                </select>
                <input type="text" style="" placeholder="Min price">
                <input type="text" style="" placeholder="Max price">
                <div id="slider-range"></div>
            </fieldset>
            <button type="submit"  class="btn btn-default btn-lg text-center">Search <br class="hidden-sm hidden-xs"> Property</button>
        </form>
    </div>
</div>




<section id="home-property-listing">
    <header class="section-header home-section-header text-center">
        <div class="container">
            <h2 class="wow slideInRight">The Beauty of Algorithm</h2>
        </div>
    </header>
    <div class="container">
        <div class="row" id="mapping_things">

        </div>
    </div>
</section>


<button onclick="viewAllModelsMultiSeller()">viewAllModelsSingleSeller</button>



<div class="cd-cart-container empty">
    <a href="#0" class="cd-cart-trigger">
        Cart
        <ul class="count"> <!-- cart items count -->
            <li>0</li>
            <li>0</li>
        </ul> <!-- .count -->
    </a>

    <div class="cd-cart">
        <div class="wrapper">
            <header>
                <h2>Cart</h2>
                <span class="undo">Item removed. <a href="#0">Undo</a></span>
            </header>
            <div class="body">
                <ul>
                    <!-- products added to the cart will be inserted here using JavaScript -->
                </ul>
            </div>
            <footer>
                <a href="#0" class="checkout btn"><em>Checkout - $<span>0</span></em></a>
            </footer>
        </div>
    </div> <!-- .cd-cart -->
</div>


<a href="#top" id="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/jquery.migrate.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="http://ditu.google.cn/maps/api/js?key=AIzaSyD2MtZynhsvwI2B40juK6SifR_OSyj4aBA&libraries=places"></script>




<script src="js/vendor/jquery-1.11.2.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

<script src="js/vendor/bootstrap.min.js"></script>

<script src="js/plugins.js"></script>
<script src="js/main.js"></script>


<script language="javascript" type="text/javascript" src="js/web3.js"></script>
<script language="javascript" type="text/javascript" src="SellerABI.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('.scroll-link').on('click', function(event){
            event.preventDefault();
            var t = $(window).scrollTop();
            $('body,html').animate({'scrollTop':t+900},1000)
        });
        viewAllModelsMultiSeller();
        // navigation click actions
        $('.shop').on('click', function(event){
            event.preventDefault();
            window.location.href=("shopping.html");
        });
        $('.sell').on('click', function(event){
            event.preventDefault();
            window.location.href=("selling.html");
        });
        // scroll to top action
        $('.scroll-top').on('click', function(event) {
            event.preventDefault();
            window.location.href=("index.html");
        });
        // mobile nav toggle
        $('#nav-toggle').on('click', function (event) {
            event.preventDefault();
            $('#main-nav').toggleClass("open");
        });
        $('.scroll-profile').on('click', function(event){
            event.preventDefault();
            window.location.href=("profile.html");
        });
        $('.logout').on('click', function(event){
            event.preventDefault();
            window.location.href=("index.html");
        });
    });
    // scroll function
//    function scrollToID(id, speed){
//        var offSet = 50;
//        var targetOffset = $(id).offset().top - offSet;
//        var mainNav = $('#main-nav');
//        $('html,body').animate({scrollTop:targetOffset}, speed);
//        if (mainNav.hasClass("open")) {
//            mainNav.css("height", "1px").removeClass("in").addClass("collapse");
//            mainNav.removeClass("open");
//        }
//    }
    if (typeof console === "undefined") {
        console = {
            log: function() { }
        };
    }


    if (typeof web3 !== 'undefined') {
        web3 = new Web3(web3.currentProvider);
    } else {
        web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545")); // Ganache
        // web3 = new Web3(new Web3.providers.HttpProvider("https://ropsten.infura.io/6M2h6tjoj7J0gINrcSzo")); // Ropsten
    }

    // Insert ABI
    let SellerABI = web3.eth.contract(SellerABIContent);
    let allSellers = new Array('0x895d5459e94982bf2a4e73a301ba7cc88fcfe169', '0xdfceed1b4b56d4e833defba5a05a3d5cac13e0aa');
    let allSellerConstracts = new Array('0x6dcaaf9fa4966d89d93e26053b52350b7bc6d651', '0x1673591bb30e008e11d82492d8c04debd8bdd102');



    function viewAllModelsMultiSeller() {
        if (typeof web3 !== 'undefined') {
            web3 = new Web3(web3.currentProvider);
        } else {
            web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545")); // Ganache
            // web3 = new Web3(new Web3.providers.HttpProvider("https://ropsten.infura.io/6M2h6tjoj7J0gINrcSzo")); // Ropsten
        }

        // try to get model count
        let Seller_contract0 = SellerABI.at(allSellerConstracts[0]);
        Seller_contract0.getModelCount.call({from:web3.eth.defaultAccount}, function (error, result) {
            if (!error) {
                console.log("The result : " + result);
            }
            else{
                console.log(error);
            }
        });



        $("#mapping_things").empty();
        for (let j = 0; j < allSellers.length; j++){
            // Get access to the contract
            web3.eth.defaultAccount = allSellers[j];
            let Seller_contract = SellerABI.at(allSellerConstracts[j]);


            var modelCount = Seller_contract.getModelCount();
            for(var i = 0; i < modelCount; i++){
                // get model information
                var currentModel = Seller_contract.getModelByIndex(i);
                $("#mapping_things").append(`<div class="col-lg-4 col-sm-6 layout-item-wrap">
                    <article class="property layout-item clearfix">
                            <figure class="feature-image">
                            <a class="clearfix zoom"><img src="http://www.writtalin.com/wp-content/uploads/2014/06/Screen-Shot-2014-06-05-at-7.33.51-PM-493x350.jpg"></a>
                            <span class="btn btn-warning btn-sale">stared</span>
                            </figure>
                            <div class="property-contents clearfix">
                            <header class="property-header clearfix">
                            <div class="pull-left">
                            <h6 class="entry-title"><a >Big tits</a></h6>
                        <span class="property-location"><i class="fa fa-map-marker"></i>`+currentModel[2]+`</span>
                            </div>
                            <a href="#0" class="cd-add-to-cart" data-price="389">456</a>
                        </header>
                        <div class="property-meta clearfix">
                            <span><i class="fa fa-arrows-alt"></i> Ai</span>
                            <span><i class="fa fa-arrows-alt"></i> Hard</span>
                            <span><i class="fa fa-arrows-alt"></i> 3 days left</span>
                        <span><i class="fa fa-arrows-alt"></i> With data</span>
                        </div>
                        </div>
                        </article>
                        </div>`);

            }
        }
    }
</script>

</body>
</html>


