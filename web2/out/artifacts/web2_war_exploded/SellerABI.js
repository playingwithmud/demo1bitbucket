// ABI : remix compile -> details -> copy value to clipboard
// For same contract (Seller.sol) the ABI will be same as well
var SellerABIContent =[
    {
        "constant": false,
        "inputs": [],
        "name": "test_posts_5models",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "constant": false,
        "inputs": [],
        "name": "getModelCount",
        "outputs": [
            {
                "name": "",
                "type": "uint256"
            }
        ],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [
            {
                "name": "_modelSellerIndex",
                "type": "uint256"
            }
        ],
        "name": "getModelByIndex",
        "outputs": [
            {
                "name": "",
                "type": "address"
            },
            {
                "name": "",
                "type": "string"
            },
            {
                "name": "",
                "type": "bytes32"
            },
            {
                "name": "",
                "type": "bytes32"
            },
            {
                "name": "",
                "type": "uint256"
            },
            {
                "name": "",
                "type": "uint256"
            },
            {
                "name": "",
                "type": "uint256"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "constant": false,
        "inputs": [],
        "name": "test_posts_20models",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "constant": false,
        "inputs": [
            {
                "name": "_modelSellerIndex",
                "type": "uint256"
            },
            {
                "name": "_newPrice",
                "type": "uint256"
            }
        ],
        "name": "updateModel",
        "outputs": [
            {
                "name": "",
                "type": "bool"
            }
        ],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "constant": false,
        "inputs": [
            {
                "name": "_modelName",
                "type": "string"
            },
            {
                "name": "_modelDescriptionAddr",
                "type": "bytes32"
            },
            {
                "name": "_modelAddr",
                "type": "bytes32"
            },
            {
                "name": "_modelPrice",
                "type": "uint256"
            }
        ],
        "name": "postModel",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [
            {
                "name": "",
                "type": "uint256"
            }
        ],
        "name": "postedModels",
        "outputs": [
            {
                "name": "modelName",
                "type": "string"
            },
            {
                "name": "modelDescriptionAddr",
                "type": "bytes32"
            },
            {
                "name": "modelAddr",
                "type": "bytes32"
            },
            {
                "name": "modelPrice",
                "type": "uint256"
            },
            {
                "name": "modelTime",
                "type": "uint256"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "inputs": [],
        "payable": true,
        "stateMutability": "payable",
        "type": "constructor"
    },
    {
        "payable": true,
        "stateMutability": "payable",
        "type": "fallback"
    },
    {
        "anonymous": false,
        "inputs": [
            {
                "indexed": false,
                "name": "_sellerAddr",
                "type": "address"
            },
            {
                "indexed": false,
                "name": "_modelName",
                "type": "string"
            },
            {
                "indexed": false,
                "name": "_modelDescriptionAddr",
                "type": "bytes32"
            },
            {
                "indexed": false,
                "name": "_modelAddr",
                "type": "bytes32"
            },
            {
                "indexed": false,
                "name": "_modelPrice",
                "type": "uint256"
            },
            {
                "indexed": false,
                "name": "_modelTime",
                "type": "uint256"
            },
            {
                "indexed": false,
                "name": "_modelSellerIndex",
                "type": "uint256"
            }
        ],
        "name": "ModelPosted",
        "type": "event"
    },
    {
        "anonymous": false,
        "inputs": [
            {
                "indexed": false,
                "name": "_sellerAddr",
                "type": "address"
            },
            {
                "indexed": false,
                "name": "_modelName",
                "type": "string"
            },
            {
                "indexed": false,
                "name": "_modelDescriptionAddr",
                "type": "bytes32"
            },
            {
                "indexed": false,
                "name": "_modelAddr",
                "type": "bytes32"
            },
            {
                "indexed": false,
                "name": "_modelPrice",
                "type": "uint256"
            },
            {
                "indexed": false,
                "name": "_modelTime",
                "type": "uint256"
            },
            {
                "indexed": false,
                "name": "_modelSellerIndex",
                "type": "uint256"
            }
        ],
        "name": "ModelUpdated",
        "type": "event"
    },
    {
        "anonymous": false,
        "inputs": [
            {
                "indexed": false,
                "name": "_modelCount",
                "type": "uint256"
            }
        ],
        "name": "ModelCount",
        "type": "event"
    },
    {
        "anonymous": false,
        "inputs": [
            {
                "indexed": false,
                "name": "_sellerAddr",
                "type": "address"
            },
            {
                "indexed": false,
                "name": "_modelName",
                "type": "string"
            },
            {
                "indexed": false,
                "name": "_modelDescriptionAddr",
                "type": "bytes32"
            },
            {
                "indexed": false,
                "name": "_modelAddr",
                "type": "bytes32"
            },
            {
                "indexed": false,
                "name": "_modelPrice",
                "type": "uint256"
            },
            {
                "indexed": false,
                "name": "_modelTime",
                "type": "uint256"
            },
            {
                "indexed": false,
                "name": "_modelSellerIndex",
                "type": "uint256"
            }
        ],
        "name": "ModelByIndex",
        "type": "event"
    }
];
