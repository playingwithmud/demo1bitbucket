<%@ page language="java" import="java.util.*,com.mongodb.*" pageEncoding="UTF-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>My JSP 'register_action.jsp' starting page</title>

    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta http-equiv="description" content="This is my page">

</head>

<body>
<%
    response.setContentType("text/html;charset=utf-8");
    request.setCharacterEncoding("utf-8");
    String userName1=(String)request.getParameter("u");
    String passWord1=(String)request.getParameter("p");
    String passWord2=(String)request.getParameter("rep");
    if(!passWord1.equals(passWord2)){
        response.sendRedirect("signup.jsp");
    }

    try{
        MongoClient mongoClient = new MongoClient( "localhost" , 27017 );
        @SuppressWarnings("deprecation")
        DB db = mongoClient.getDB( "test" );
        DBCollection coll = db.getCollection("site");
        System.out.println("Collection userInfo selected successfully");

        DBObject user = new BasicDBObject();
        user.put("name", userName1);
        user.put("password", passWord1);
        coll.insert(user);
        response.sendRedirect("start.jsp");

    }catch(Exception e){
        System.err.println( e.getClass().getName() + ": " + e.getMessage() );
    }
%>


</body>
</html>