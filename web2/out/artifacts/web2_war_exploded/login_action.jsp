<%@ page language="java" import="java.util.*,com.mongodb.*,com.mongodb.MongoClient" pageEncoding="UTF-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>My JSP 'login_action.jsp' starting page</title>

    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
    <!--
    <link rel="stylesheet" type="text/css" href="styles.css">
    -->

</head>

<body>
<%
    response.setContentType("text/html;charset=utf-8");
    request.setCharacterEncoding("utf-8");
    String userName=(String)request.getParameter("u");
    String passWord=(String)request.getParameter("p");
    String checkBox = request.getParameter("save_password");
    boolean login_test = false;
    try{
        MongoClient mongoClient = new MongoClient( "localhost" , 27017 );
        DB db = mongoClient.getDB( "test" );
        DBCollection coll = db.getCollection("site");
        System.out.println("Collection userInfo selected successfully");
        DBCursor cursor = coll.find();

        int i=1;
        while (cursor.hasNext()) {
            System.out.println("userInfo Document: "+i);
            DBObject show = cursor.next();
            System.out.println(show);
            Map show1 = show.toMap();
            String toname = (String)show1.get("name");
            String topassword = (String)show1.get("password");
            if(toname.equals(userName) && topassword.equals(passWord)){
                System.out.println("登陆成功！！！！！"+"username:"+toname+"  password:"+topassword);
                login_test = true;
            }
            i++;
        }

    }catch(Exception e){
        System.err.println( e.getClass().getName() + ": " + e.getMessage() );
    }

    if(login_test) {
        if ("save".equals(checkBox)) {
            String name1 = java.net.URLEncoder.encode(userName,"UTF-8");
            Cookie nameCookie = new Cookie("username", name1);
            nameCookie.setMaxAge(60 * 60 * 24 * 3);

            String pwd = java.net.URLEncoder.encode(passWord,"UTF-8");
            Cookie pwdCookie = new Cookie("password", pwd);
            pwdCookie.setMaxAge(60 * 60 * 24 * 3);
            response.addCookie(nameCookie);
            response.addCookie(pwdCookie);
        }
        response.sendRedirect("start.jsp");
    }
    else{
        response.sendRedirect("login.jsp");
    }
%>
</body>
</html>