// for (let i=0; i<5; i++) {
//     // var temp = i;
//     setTimeout(function(){
//         console.log(i); 
//     }, i);
//     // console.log("Temp : " + temp);
// }  

// String matching
let sentence = "Target-driven visual navigation in indoor scenes using deep reinforcement learning";
let pattern = "target";
let sentence_lower = sentence.toLowerCase();
let pattern_lower = pattern.toLowerCase();
console.log(sentence_lower);
console.log(pattern);
if (sentence_lower.match(pattern_lower)) {
    console.log("Match!");
}
else {
    console.log("No match!");
}
