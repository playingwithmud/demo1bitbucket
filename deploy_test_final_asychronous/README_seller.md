## Deployment test : Seller

+ Update on 180418
    + Contract : Change the type of address to string
    + Contract : Change postModel and updateModel, add time
    + Deployment : Time conversion
    + Deployment : Currency conversion

+ Multiple sellers on both Ropsten Testnet and Ganache
    + 2 Sellers, one with 5 models while another with 20 models
    + Ganache: passed all tests
    + Note that before running you should reset the address for sellers and contracts
+ Tasks:
    + Test 1: View all sellers' address as well as contracts address
    + Test 2: Post a model
        + Seller's address
        + Contract's address
        + Model's information
    + Test 3: View the number of models
        + Input : Seller's address, Contract's address
        + Output : Number of model
    + Test 4: Find a model's information
        + Input : Seller's address, Contract's address, The model's index
        + Then return all information of this model
    + Test 5: View all models
    + Test 6: Update a model
        + Input : Seller's address, Contract's address, The model's index
    + Test 7: View one seller's models
        + Input : Seller's address, Contract's address
 + Current problems:
    + Ropsten: **Infura is incompetible with sendTransaction()**
    + I don't know how to use `require()`
