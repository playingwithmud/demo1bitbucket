## Deployment test : Buyer

+ Update on 180418
    + Contract : change the type of address to string
    + Contract : check repeat (address)
    + Contract : change buyModel, add time
    + Deployment : check repeat before buying, if this model has been bought, buyModel will not be lanuched
    + Deployment : Time conversion
    + Deployment : Currency conversion

+ Multiple buyers on both Ropsten Testnet and Ganache
    + There are 2 Sellers, one with 5 models while another with 20 models
    + There are 2 Buyers
    + Note that before running on Ganache you should reset the address for both buyers and sellers as well as their contracts
+ Tasks:
    + Test 1: View all buyers' address as well as contracts address
    + Test 2: Buy a model
        + When the buyer buys a model, the input should be the seller address, seller contract address, index of this model
        + Get access to seller's contract then view the details of model to be bought
        + Get access to buyer's contract
        + Deposit money to buyer's contract
        + Buy model, the money will be transferred from buyer's contract to seller
        + Then this transaction is finished
    + Test 3: View the number of models I bought
        + Input : Buyer's address, Contract's address
        + Output : Number of model bought by this buyer
    + Test 4: View all models I bought
        + Input : Buyer's address, Contract's address
        + Output : All models bought by this buyer
    + Test 5: View my balance
        + Input : Buyer's address, Contract's address
        + Output : the balance of buyer
    + Test 6: Search for model by name
        + Input : the keyword
        + Output : the search result
 + Test result:
    + Finished testing on Ganache
