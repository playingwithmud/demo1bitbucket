# Contract generation [Finished by Yunqiu Xu]
+ In this test, I will try to create the contracts
+ When a user is created, his seller and buyer contract will also be initialized automatically
+ This part takes user's metamask address and return the address of 2 contracts
## How to run
+ Before running you should check there are .abi files and .bin files, if not, run following code to compile them from .sol files
```
solcjs --abi Seller_submit.sol
solcjs --bin Seller_submit.sol
solcjs --abi Buyer_submit.sol
solcjs --bin Buyer_submit.sol
```
+ Then run the .js
```
node deployContract.js file
```
+ You should see the address being printed, and on Ganache / TestRPC you can see the new transactions
+ You can change the users by commenting them in the js file

## If you want to modify the contract
+ Remove old bins and ABIs first
```
sudo rm -rf *.bin
sudo rm -rf *.abi
```
+ Modify the contracts
+ Rrbuild ABIs and bins
+ Run again
