// [Yunqiu Xu] Contract generation
// Ref:
//      https://web3js.readthedocs.io/en/1.0/web3-eth-contract.html#deploy
//      https://medium.com/mercuryprotocol/dev-highlights-of-this-week-cb33e58c745f 



const fs = require("fs");
const solc = require('solc');
let Web3 = require('web3');

// [Yunqiu Xu] This is the input (user's metamask address)

// [Xingshi Zhang] modify this as input
// Test account for seller
// let userAddr = '0x627306090abaB3A6e1400e9345bC60c78a8BEf57';
// let userAddr = '0xf17f52151EbEF6C7334FAD080c5704D77216b732';
// Test account for buyer
// let userAddr = '0xc5fdf4076b8f3a5357c5e395ab970b5b54098fef';
// let userAddr = '0x821aEa9a577a9b44299B9c15c88cf3087F3b5544';

// [Xingshi Zhang] Change to 
// Set provider
let web3 = new Web3();
web3.setProvider(new web3.providers.HttpProvider('http://localhost:7545'));

// Get ABI
let SellerABI = JSON.parse(fs.readFileSync('Seller_submit_sol_Seller.abi', 'utf8'));
let BuyerABI = JSON.parse(fs.readFileSync('Buyer_submit_sol_Buyer.abi','utf8'));

// [Can use local file] Get Bytecode
let byteCodeHead = '0x';
let SellerByteCode = byteCodeHead + fs.readFileSync('Seller_submit_sol_Seller.bin','utf8');
let BuyerByteCode = byteCodeHead + fs.readFileSync('Buyer_submit_sol_Buyer.bin', 'utf8');

// Get contract instance
let sellerContract = new web3.eth.Contract(SellerABI, null, {
    from: userAddr,
    gas: 3000000,
    data: SellerByteCode
});
let buyerContract = new web3.eth.Contract(BuyerABI, null, {
    from: userAddr,
    gas: 3000000,
    data: BuyerByteCode
});

// Deploy seller
sellerContract.deploy()
.send({
    from: userAddr,
    gas: 3000000,
    gasPrice: '100'
})
.then(function(newContractInstance){
    console.log("Seller contract's address : " + newContractInstance.options.address)
});

// Deploy buyer
buyerContract.deploy()
.send({
    from: userAddr,
    gas: 3000000,
    gasPrice: '100'
})
.then(function(newContractInstance){
    console.log("Buyer contract's address : " + newContractInstance.options.address)
});
